package week3.domain;

import java.util.regex.Pattern;

public class Password {
    // passwordStr // it should contain uppercase and lowercase letters and digits
        // and its length must be more than 9 symbols
        private String passwordStr;

        public Password(String passwordStr) {
            setPasswordStr(passwordStr);
        }

        public String getPasswordStr(){
            return passwordStr;
        }
        public void setPasswordStr(String passwordStr){
            if(CheckPassFormat(passwordStr)){
                this.passwordStr=passwordStr;
            }else{
                System.out.println("Password.setPasswordStr : Incorrect password format:" + passwordStr);
            }
        }
        public static boolean CheckPassFormat(String password) {

            Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
            Pattern lowerCasePatten = Pattern.compile("[a-z ]");
            Pattern digitCasePatten = Pattern.compile("[0-9 ]");

            boolean CheckPass = true;

            if (password.length() < 10) {
                System.out.println("Password's  length must be more than 9 symbols");
                CheckPass = false;
            }
            if (!UpperCasePatten.matcher(password).find()) {
                System.out.println("Password must have at least one uppercase");
                CheckPass = false;
            }
            if (!lowerCasePatten.matcher(password).find()) {
                System.out.println("Password must have at least one lowercase");
                CheckPass = false;
            }
            if (!digitCasePatten.matcher(password).find()) {
                System.out.println("Password must have at least one digit");
                CheckPass = false;
            }


            return CheckPass;

        }
    }
