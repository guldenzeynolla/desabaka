package week3.domain;

import java.util.ArrayList;

public class User {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private static String username;
    private static Password password;
    private ArrayList<User> listOfUsers;


    public User() {
        id = id_gen++;
    }

    public User(String name, String surname, String username, Password password) {
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }

    public User(int id, String name, String surname, String username, Password password) {
        setId(id);
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        updateIdgen(id);
    }

    private static void updateIdgen(int id) {
        if (id_gen < id + 1) {
            id_gen = id + 1;

        }


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public static String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    @Override
    public String toString() {

        return id + " " + name + " " + surname + " " + username + " " + password;

    }
}