package week3.domain;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    private ArrayList<User> listOfUsers;
    private Scanner sc = new Scanner(System.in);
    private User signedUser;

    private void addUser(User user) {
        listOfUsers.add(user);
    }

    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile();
            }
        }
    }

    private void userProfile() {
        while (true) {
            System.out.println("Welcome to your profile!");
            System.out.println("If you want to log out, select 1");
            System.out.println("If you want to stay here, select 2");

            int choice = sc.nextInt();
            if (choice == 1) {
                logOff();
            }else if (choice == 2) {
                break;
            }
        }
    }

    private void logOff() {
        signedUser = null;
        menu();

    }

    private void authentication() {
        while (true) {
            System.out.println("Welcome to authentication!");
            System.out.println("Select command:");
            System.out.println("1. signIn");
            System.out.println("2. signUp");
            System.out.println("3. Exit");

            int choice = sc.nextInt();
            if (choice == 1) {
                signIn();
            } else if(choice == 2) {
                signUp();
            }
        }
    }

    private void signIn() {

        System.out.println("Username");
        String username = sc.nextLine();
        System.out.println("Password");
        String password = sc.nextLine();
        Password password1 = new Password(password);
        //        for (int i = 0; i < listOfUsers.size(); i++) {
        for (User user :listOfUsers) {
            if(username.equals(User.getUsername()) && password1.equals(User.getPassword())){
                signedUser = user;
            } else {
                System.out.println("Unable to log in.");
                System.out.println("Please check the spelling of your login and password.");
                System.out.println("To try again, press 1");
                System.out.println("To end the attempt, press 1");
                int choice = sc.nextInt();
                if (choice == 1) {
                    signIn();
                } else {
                    return;
                }
            }
        }
    }

    private void signUp() {
        User newUser = new User();
        Scanner sc = new Scanner(System.in);
        String name, surname, username, password;
        System.out.println("  What is your name? ");
        name = sc.nextLine();
        System.out.println("What is your surname? ");
        surname = sc.nextLine();
        System.out.println("Choose your username: ");
        username = sc.nextLine();
         if (!UsernameExist(username)) {
             System.out.println("This username already exists!");
         }

         System.out.println("If you want to try again , select 1");
         System.out.println("If you understand that you already have an account, select 2");
            int choice = sc.nextInt();
            if (choice == 1) {
                signUp();
            } else if (choice == 2){
                signIn();
            }else{
                return;
            }

        System.out.println("Create the password: ");
        password = sc.nextLine();
        Password password1 = new Password(password);
        newUser.setPassword(password1);
        addUser(newUser);
        signedUser = newUser;

        System.out.println("Access!");


    }

    private boolean UsernameExist(String username) {

        for (int i = 0; i < listOfUsers.size(); i++) {
            if (username.equals(User.getUsername())) {
                return true;
            }
        }
        return false;
    }


    public void start() throws FileNotFoundException {
        File file = new File("/Users/guldenzeynolla/Downloads/db.txt");
        Scanner fileScanner = new Scanner(file);
/* Понять как fill txt, этот код неверный и с инета не обманывай себя бро
*
        try {

            String data = Scanner(file);

            File file = new File("/Users/guldenzeynolla/Downloads/db.txt");

            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            bw.write(data);

            System.out.println("The list of users is filled");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }

    }
    */
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        saveUserList();
    }

    private void saveUserList() {
        String content = "";
        for(User user :listOfUsers){
            content += user + "\n";
        }
        Files.write(Paths.get("/Users/guldenzeynolla/Downloads/db.txt"));
    }
}

